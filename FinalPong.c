/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by Estefania Valencia Navarrete
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................
    // NOTE: Here there are some useful variables (should be initialized)
    float width = 30;
    float height = 100;
    
    int playerPosX = 10;
    int playerPosY = screenHeight/2 - height /2;
    Rectangle player = {playerPosX, playerPosY, width, height};
    int playerSpeedY = 10;
    
    Rectangle enemy = {screenWidth - 10 - width, screenHeight/2 - height/2, width, height};
    int enemySpeedY = 10;
    int visionEnemy = screenWidth - 500;
    
    Color ballColor = BLACK;
    Color playerColor = GREEN;
    Color enemyColor = BLUE;
    
    Vector2 ballPosition = {screenWidth/2, screenHeight/2};
    Vector2 ballSpeed = { 4, 8 };
    float ballRadius = 30;
    
    int playerLife = 5;
    int enemyLife = 5;
    
    int secondsCounter = 99;
    
    int framesCounter = 0; // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............
                
                framesCounter++;
            
                if (framesCounter > 240) screen = TITLE;
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................
                framesCounter++;                                                                
                
                // TODO: "PRESS ENTER" logic.........................                                                               
                if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!

                // TODO: Ball movement logic.........................                               
                // if (ballPosition.x < ballRadius) 
                // {
                    // ballPosition.x = ballRadius;
                    // ballSpeed.x = ballSpeed.y * -1;
                // }
                
                if (ballPosition.y < ballRadius)
                {
                    ballPosition.y = ballRadius;
                    ballSpeed.y = ballSpeed.y * -1;
                }
                
                if (ballPosition.x > screenWidth - ballRadius)
                {
                    ballPosition.x = screenWidth - ballRadius;
                    ballSpeed.x = ballSpeed.x * -1;
                }
                
                if (ballPosition.y > screenHeight - ballRadius)
                {
                    ballPosition.y = screenHeight - ballRadius;
                    ballSpeed.y = ballSpeed.y * -1;
                }
                
                // TODO: Player movement logic.......................
                if (IsKeyDown(KEY_UP)){
                    if (playerPosY < 0)
                    {
                        playerPosY = 0;
                    }else{
                        playerPosY = playerPosY - playerSpeedY;   
                    }                                   
                }
                
                if (IsKeyDown(KEY_DOWN)){
                    if (playerPosY > screenHeight - height)
                    {
                        playerPosY = screenHeight - height;
                    }else{
                        playerPosY = playerPosY + playerSpeedY;   
                    }                                        
                }
                
                player.y = playerPosY;
                
                // TODO: Enemy movement logic (IA)...................
                if (ballPosition.x >= visionEnemy)
                {
                    if(GetRandomValue(-1, 1) > 0){
                        enemySpeedY = 6;
                    }
                    if (ballPosition.y > (enemy.y + enemy.height/2)) enemy.y += enemySpeedY;
                    if (ballPosition.y < (enemy.y + enemy.height/2)) enemy.y -= enemySpeedY;  
                    enemySpeedY = 10;                    
                }
                
                if (enemy.y <= 0) enemy.y = 0;
                else if ((enemy.y + enemy.height) >= screenHeight) enemy.y = screenHeight - enemy.height;
                
                // TODO: Collision detection (ball-player) logic.....
                if(CheckCollisionCircleRec(ballPosition, ballRadius, player))
                {
                    ballColor = playerColor;
                    ballPosition.x = 10 + width + ballRadius;
                    ballSpeed.x = ballSpeed.x * -1;
                    
                }               
                
                // TODO: Collision detection (ball-enemy) logic......
                if(CheckCollisionCircleRec(ballPosition, ballRadius, enemy))
                {
                    ballColor = enemyColor;
                    ballPosition.x = screenWidth - (width*2)- ballRadius;
                    ballSpeed.x = ballSpeed.x * -1;
                    
                }
                
                // TODO: Collision detection (ball-limits) logic.....
                if(ballPosition.x < 0 + ballRadius){
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    playerLife = playerLife - 1;
                }
                if(ballPosition.x > screenWidth - ballRadius){
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    enemyLife = enemyLife - 1;
                }
                
                // TODO: Life bars decrease logic....................
                for (int i = 0; i < playerLife; i++) DrawRectangle(20 + 40*i, screenHeight - 30, 35, 10, GREEN);
                for (int i = 0; i < enemyLife; i++) DrawRectangle(screenWidth - 50 - 40*i, screenHeight - 30, 35, 10, GREEN);

                // TODO: Time counter logic..........................
                // secondsCounter--;
                

                // TODO: Game ending logic...........................
                if(secondsCounter == 0){
                    
                    
                    screen = ENDING;
                }else{
                    if(playerLife == 0){                       
                        screen = ENDING;
                    }else if(enemyLife == 0){
                        screen = ENDING;
                    }else{
                        screen = GAMEPLAY;
                    }                    
                }
                
                // TODO: Pause button logic..........................
                
                
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................
                ballPosition.x = screenWidth/2;
                ballPosition.y = screenHeight/2;
                ballSpeed.x = 0;
                ballSpeed.y = 0;

                if (IsKeyPressed(KEY_ESCAPE)) CloseWindow();
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................
                    DrawRectangle(0, 0, screenWidth, screenHeight, DARKBLUE);
                    DrawText("FINAL PONG!", 240, screenHeight/2 - 40, 52, GRAY);
                    DrawText("wait for 4 seconds...", 320, screenHeight - 30, 15, GRAY);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................
                    const char gameTitle[31] = "WELCOME TO THE FINAL PONG GAME!";      
                    DrawText(SubText(gameTitle, 0, (framesCounter - 240)/10), 210, 160, 20, MAROON);    
                    
                    // TODO: Draw "PRESS ENTER" message..............
                    DrawText("PRESS [ENTER] to START!", 240, 260, 20, LIGHTGRAY);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    // TODO: Draw player and enemy...................
                    DrawRectangleRec(player, playerColor);
                    DrawRectangleRec(enemy, enemyColor);
                    
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                    DrawCircleV(ballPosition, ballRadius, ballColor);
                    
                    // TODO: Draw player and enemy life bars.........
                    
                    // char textTimer[2];
                    // TODO: Draw time counter.......................
                    // DrawText( , 240, screenHeight - 20, 20, LIGHTGRAY);
                    
                    // TODO: Draw pause message when required........
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......
                    if(secondsCounter == 0){
                        DrawText("TIMES UP!", 360, 40, 10, LIGHTGRAY);
                        if(playerLife < enemyLife){
                            DrawText("You have lost...", 240, 20, 40, LIGHTGRAY);
                        }else if(enemyLife < playerLife){
                            DrawText("YOU WIN!", 240, 20, 40, LIGHTGRAY);
                        }else{
                            DrawText("DRAW!", 240, 20, 40, LIGHTGRAY);
                        }
                    }
                    
                    if(playerLife == 0){
                        DrawText("You have lost...", 240, 20, 40, LIGHTGRAY);
                    }else if(enemyLife == 0){
                        DrawText("YOU WIN!", 240, 20, 40, LIGHTGRAY);
                    }
                    
                    
                    DrawText("Press [ESC] to exit.", 300, screenHeight - 40, 20, LIGHTGRAY);
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}